<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\MissingValue;

class BooksCollection extends ResourceCollection {
    public $collects = BooksResource::class;

    public function toArray($request) {
        return $this->collection;
    }
}
