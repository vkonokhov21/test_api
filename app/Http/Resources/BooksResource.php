<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BooksResource extends JsonResource {

    protected $withAuthors = false;
    protected $withComments = false;

    public function withAuthors() {
        $this->withAuthors = true;
        return $this;
    }

    public function withComments() {
        $this->withComments = true;
        return $this;
    }

    public function toArray($request) {
        $item = [
            'id'          => $this->id,
            'title'       => $this->title,
            'description' => $this->description,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];

        if ($this->withAuthors) {
            $item['authors'] = new AuthorsCollection($this->authors);
        }

        if ($this->withComments) {
            $item['comments'] = new CommentsCollection($this->comments()->paginate(10));
        }

        return $item;
    }
}
