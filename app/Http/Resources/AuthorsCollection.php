<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AuthorsCollection extends ResourceCollection {
    public $collects = AuthorsResource::class;

    public function toArray($request) {
        return $this->collection;
    }
}
