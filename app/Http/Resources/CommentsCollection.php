<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommentsCollection extends ResourceCollection {
    public $collects = CommentsResource::class;

    public function toArray($request) {
        return $this->collection;
    }
}
