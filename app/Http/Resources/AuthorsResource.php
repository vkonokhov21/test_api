<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthorsResource extends JsonResource {
    protected $withBooks = false;

    public function withBooks() {
        $this->withBooks = true;
        return $this;
    }

    public function toArray($request) {
        $item = [
            'id' => $this->id,
            'name'       => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if($this->withBooks) {
            $item['books'] = new BooksCollection($this->books);
        }

        return $item;
    }
}
