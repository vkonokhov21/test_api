<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorRequest;
use App\Http\Resources\AuthorsResource;
use App\Http\Resources\AuthorsCollection;
use App\Models\Author;

class AuthorsController extends Controller {

    public function index() {
        $authors = Author::all();
        return new AuthorsCollection($authors);
    }

    public function show(Author $author) {
        return (new AuthorsResource($author))->withBooks();
    }

    public function store(AuthorRequest $request) {
        $author = Author::create([
            'name' => $request->input('name'),
        ]);

        return (new AuthorsResource($author))
            ->response()
            ->header('Location', route('authors.show', [
                'author' => $author,
            ]));
    }

    public function update(AuthorRequest $request, Author $author) {
        $author->update($request->all());
        return new AuthorsResource($author);
    }

    public function destroy(Author $author) {
        $author->delete();
        return response(null, 204);
    }
}
