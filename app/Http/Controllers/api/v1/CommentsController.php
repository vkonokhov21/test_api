<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentsResource;
use App\Models\Comment;

class CommentsController extends Controller {

    public function create(CommentRequest $request) {
        $comment = Comment::create($request->all());
        return new CommentsResource($comment);
    }
}
