<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Http\Resources\BooksCollection;
use App\Http\Resources\BooksResource;
use Illuminate\Http\Request;
use App\Models\Book;

class BooksController extends Controller {
    public function index(Request $request) {
        $books = Book::all()->paginate($request->get('perPage', 10));
        return new BooksCollection($books);
    }

    public function show(Book $book) {
        return (new BooksResource($book))->withAuthors()->withComments();
    }

    public function store(CreateBookRequest $request) {
        $book = Book::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
        ]);

        $ids = $request->input('author_ids');
        $book->authors()->sync($ids);

        return (new BooksResource($book))
            ->response()
            ->header('Location', route('books.show', [
                'book' => $book,
            ]));
    }

    public function update(UpdateBookRequest $request, Book $book) {
        $book->update($request->all());

        $ids = $request->input('author_ids');
        $book->authors()->sync($ids);

        return (new BooksResource($book))->withAuthors()->withComments();
    }

    public function destroy(Book $book) {
        $book->delete();
        return response(null, 204);
    }
}
