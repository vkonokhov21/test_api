<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBookRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'title'       => 'required|string|max:128',
            'description' => 'sometimes|required|string|max:1024',
            'author_ids'  => 'array',
        ];
    }
}
