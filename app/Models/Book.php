<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Book extends Model {

    protected $fillable = [
        'title', 'description',
    ];

    public function authors() {
        return $this->belongsToMany(Author::class, 'book_author');
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }
}
