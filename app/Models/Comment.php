<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Comment extends Model {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'book_id', 'text',
    ];

    protected $hidden = ['created_at', 'updated_at', 'book_id'];

    public function book() {
        return $this->hasOne(Book::class);
    }
}
