<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BookAuthor extends Model {

    protected $hidden = ['created_at', 'updated_at'];

    public function authors() {
        return $this->hasMany(Author::class);
    }

    public function books() {
        return $this->hasMany(Book::class);
    }

}
