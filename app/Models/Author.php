<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Author extends Model {

    protected $fillable = [
        'name',
    ];

    public function books() {
        return $this->belongsToMany(Book::class, 'book_author');
    }
}
