<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'App\Http\Controllers\API\RegisterController@register');

Route::middleware('auth:api')->prefix('v1')->group(function(){
    Route::apiResource('authors', 'App\Http\Controllers\api\v1\AuthorsController');
    Route::apiResource('books', 'App\Http\Controllers\api\v1\BooksController');

    Route::post('comments/create', 'App\Http\Controllers\api\v1\CommentsController@create');

});
